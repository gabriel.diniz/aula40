public class aula40 {  //instrução continue

    public static void main(String[] args) {
        System.out.println("Imprime os números pares de 0 a 100");
        for (int i = 0; i <100; i++){  // variavel inteira valor 0, enquanto i menor do que 100, acrescenta 1 unidade
            if (i%2==0)  // se i é divido por 2 e igual a 0
                System.out.println(i);  // imprimi valor da variavel i
            else  // se não
                continue; // continua se o número não for par

            // looping , faz a somatória de todos os números
            int a = 0, soma = 0; // variavel A igual 0, , variavel soma igual 0
            while (a<i){ // A menor do que i
                a++; // acrescenta uma unidade A
                soma+=a; //valor de soma + valor de A
            }
            System.out.println("A soma de todos os númmeros é: "+soma);  // imprime a mensagem
        }
    }
}
